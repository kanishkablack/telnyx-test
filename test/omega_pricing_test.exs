defmodule OmegaPricingTest do
  use ExUnit.Case
  doctest OmegaPricing

  import OmegaPricing

  setup do
    drop_tables()
    create_tables()
  end


  test "New product Entry testing" do
    status = process_product(%{id: 1234567, name: "Mixer", category: "Kitchen" , price: "$99.9",discontinued: true}) 
    assert status == :ok
  end

  test "New product testing with discontinued: false" do
    status = process_product(%{id: 7234567, name: "ToothPaste", category: "Health" , price: "$12.9",discontinued: false}) 
    assert status == false
  end

  test "ID and Name miss match testing" do
    status = process_product(%{id: 1234567, name: "Mixer", category: "Kitchen" , price: "$99.9",discontinued: true}) 
    assert status == :ok
    omni_status = process_product(%{id: 1234567, name: "Jeans", category: "Kitchen" , price: "$99.9",discontinued: false}) 
    assert omni_status == {:error,:miss_match}
  end

  test "no update in the price testing" do
    status = process_product(%{id: 4434567, name: "Shirt", category: "Clothes" , price: "$99.9",discontinued: true}) 
    assert status == :ok
    omni_status = process_product(%{id: 4434567, name: "Shirt", category: "Clothes" , price: "$99.9",discontinued: true}) 
    assert omni_status == {:ok,:no_update}
  end
end
