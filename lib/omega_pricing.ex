defmodule OmegaPricing do
  require Logger


  def start() do
    generate_url
    |> fetch
    |> parsing_data
    |> Enum.each(&process_record/1)
  end


  @doc ~S"""
  This is used to make get request with given url
  """
  def fetch(url)do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
       body
      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.error "request failed reason : #{reason}"
        {:error,"request failed"}
    end
  end


  @doc """
  Parse the data into Elixir Term Map
  """
  def parsing_data(data) do
    case Poison.decode data do
      {:ok, payload} ->
        payload["productRecords"]
      {:error, reason} ->
        Logger.warn "decoding failed"
        []
    end
  end
  

  @doc """
  Converts the map keys to atoms for convenience
  """
  def process_record(data) do
    data
    |>Enum.reduce(%{}, fn ({key, val}, acc) -> Map.put(acc, String.to_atom(key), val) end)
    |>process_product
  end



#Actual logic definition for implicit conditions 

  def process_product(%{id: id,name: name,category: category,price: price, discontinued: discontinued }) do
    if is_product_exist?(id) do              #checking the product id is present or not
      product = get_product(id)

      {_ , product_name } = List.keyfind(product,:product_name,0)
      {_, in_price } = List.keyfind(product,:price,0)

      #checking equality the product_name 
      if(product_name == name) do

        #checking the price is different or not
        if price_to_number(price)*100 != in_price do 
          change = (price_to_number(price)*100 - in_price)/100
          IO.puts "records updated #{inspect [id, name,price,change]}"

          #since the price id different updating the price here
          update_price price_to_number(price)*100,id

          #  new time past record is inserted with new price value  
          insert_pastrecords([id,price_to_number(price)*100,change,now(),now()])
          :ok
        else
          #simply stay nothing as no change in the price of the product
          {:ok,:no_update }
        end
      else
        # warning if the same id for different products get matched
        Logger.warn "proudct id & name mismatch"
        {:error,:miss_match}
      end
    else

      #if the discontinued value is true then it inserts the data
      discontinued && insert_product [id,price_to_number(price)*100,name,now(),now()]
    end
  end


###########################################################################
# Database table related definitions 
###########################################################################


# inserts the new product into products table
  def insert_product(data) do
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db, "insert into products (external_product_id,price,product_name,created_at,updated_at) values($1,$2,$3,$4,$5)",bind: data )
    end)
    Logger.info "New product inserted"
    :ok
   end 


#insert the record into pastrecords table
  def insert_pastrecords(data) do
    IO.inspect data
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db, "insert into pastrecords (product_id,price,change,created_at,updated_at) values ($1,$2,$3,$4,$5)",bind: data )
    end)
    :ok
 end 


#get the product details of the given id
   def get_product(id) do
    query_string = "select * from products where external_product_id = #{id}"
    case query(query_string) do
      {:ok,[]}-> nil
      {:ok,[data]} -> data
    end
   end


#updates the price of the record that matches the given id
   def update_price(price,id) do
    query_string = "update products set price=#{price} where external_product_id = #{id}"
    query query_string
    :ok
   end 


#executes the passed query and returns the result
  def query(query) do
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db,query)
    end)
   end 


#used to drop specific table 
  def drop(table_name) do
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db, "drop table #{ table_name }")
    end)
 end 


# drops products and pastrecords tables
  def drop_tables() do
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db, "drop table if exists products ")
      Sqlitex.query(db, "drop table if exists pastrecords ")
    end)
 end 


#checks whether the product with given id existed or not and return the boolean
 defp is_product_exist?(id) do
  get_product(id) && true || false
 end

#creates two tables of products and pastrecords
  def create_tables() do
    Sqlitex.with_db('db.db', fn(db) ->
      Sqlitex.query(db, "create table if not exists products(external_product_id integer primary key,price real,product_name text,created_at date,updated_at date)")
      Sqlitex.query(db, "create table if not exists pastrecords(product_id integer,price real,change real ,created_at date,updated_at date, foreign key(product_id) references products(external_product_id)) ")
    end)
 end 




####################################
# Utility Functions
####################################

#returns the iso format of current date 
  def now do
    Date.utc_today |> Date.to_iso8601
  end

#converts "$23.45" to 23.45
  def price_to_number(price) do
   "$" <> value =  price
    String.to_float value
  end

#Fetching today and a month ago dates
  def get_dates() do
   # Timex.d 
   today = Timex.now |> Timex.to_date
   month_ago = today |> Timex.shift(days: -30)
   [today,month_ago]
  end

#Generates url with attaching the url parameters
  def generate_url  do
    url  = Application.get_env :omega_pricing,:url
    api_key  = Application.get_env :omega_pricing,:api_key
   [today,month_ago] = get_dates()
    url <> "?api_key=#{api_key}&start_date=#{today}&end_date=#{month_ago}" 
  end

end    # end of module
