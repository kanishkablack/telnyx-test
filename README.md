# OmegaPricing

## Usage

```elixir

    config :omega_pricing, api_key: "API1234",url: "https://omegapricinginc.com/pricing/records.json"

OmegaPricing.start
```

## Installation

```elixir
def deps do
  [{:omega_pricing, "~> 0.1.0"}]
end
```

## External Modules
  1. Poison for JSON Parsing
  2. HTTPPoison for HTTP Request Handling
  3. Timex for Date Time 
  4. Logger for Log  Entries 
  5. config for Api Key and Url 

## Assumption
1. $1 = 100 pennies
2. Database used: SQLITE
3. Initial Stage: Tables Created


```sql
  create table if not exists products(external_product_id integer primary key,price real,product_name text,created_at date,updated_at date)

  create table if not exists pastrecords(product_id integer,price real,change real ,created_at date,updated_at date, foreign key(product_id) references products(external_product_id)) 
```
## Testing
```
mix test
```

## Past Records
Assuming the creation day and updating day will be the same.

## Alternatives
1. SQL Syntax can be handled through Ecto.
2. Http Poison can be handled through Erlang Native Http CLient

Note: All Lines of Code (LOC) are Based on the knowledge of our understanding  requirements provided.

> Happy Coding!